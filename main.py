from lib import Utils, DatasetSplitter, DatasetDescriptor, augment

from datetime import datetime
from enum import Enum

import tensorflow as tf
from tensorflow.keras import Sequential
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.regularizers import l2
from tensorflow.keras.callbacks import ModelCheckpoint

import logging

logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)


class Config(Enum):
    IMAGE_SHAPE = (240, 240, 3)
    NUM_CLASSES = 6

    TRAIN_BATCH_SIZE = 4
    VALIDATE_BATCH_SIZE = 2
    TEST_BATCH_SIZE = 2

    TRAIN_STEPS = 4
    VALIDATE_STEPS = 2
    TEST_STEPS = 2

    EPOCHS = 4
    EPOCHS_PER_CHECKPOINT = 2


def augment_train_from_fp(meta_df, dataset_fp, batch_size,
                          x_col='filename',
                          y_col='labels',
                          class_mode='categorical',
                          target_size=(512, 384)):
    gen_images = ImageDataGenerator(rescale=1.0 / 255.0,
                                    rotation_range=90,

                                    horizontal_flip=True, vertical_flip=True,
                                    fill_mode='nearest', width_shift_range=.2, height_shift_range=.2)
    return gen_images.flow_from_dataframe(meta_df, dataset_fp, x_col, y_col,
                                          class_mode=class_mode,
                                          batch_size=batch_size,
                                          target_size=target_size)


def augment_test_from_fp(meta_df, dataset_fp, batch_size,
                         x_col='filename',
                         y_col='labels',
                         class_mode='categorical',
                         target_size=(512, 384)):
    gen_images = ImageDataGenerator(rescale=1.0/255.0)
    return gen_images.flow_from_dataframe(meta_df, dataset_fp, x_col, y_col,
                                          class_mode=class_mode,
                                          batch_size=batch_size,
                                          target_size=target_size)


def compile_model(input_shape, num_classes):
    model = Sequential()
    # 1st CNN layer
    model.add(tf.keras.layers.Conv2D(96, (11, 11), strides=4, activation='relu', input_shape=input_shape))
    model.add(tf.keras.layers.MaxPool2D((3, 3), strides=2))
    model.add(tf.keras.layers.BatchNormalization())

    # 2nd CNN layer
    model.add(tf.keras.layers.Conv2D(256, (5, 5), strides=1, activation='relu'))
    model.add(tf.keras.layers.MaxPool2D((3, 3), strides=2))
    model.add(tf.keras.layers.BatchNormalization())

    # 3rd CNN layer
    model.add(tf.keras.layers.Conv2D(384, (3, 3), activation='relu', strides=1))

    # 4th CNN layer
    model.add(tf.keras.layers.Conv2D(384, (3, 3), activation='relu', strides=1))

    # 5th CNN layer
    model.add(tf.keras.layers.Conv2D(256, (3, 3), activation='relu', strides=1))
    model.add(tf.keras.layers.MaxPool2D((3, 3), strides=2))
    model.add(tf.keras.layers.BatchNormalization())

    # Dropout layer
    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dropout(0.5))

    # Fully connected layer 1
    model.add(tf.keras.layers.Dense(4096, activation='relu', kernel_regularizer=l2(l=0.01)))
    model.add(tf.keras.layers.Dropout(0.5))

    # Fully connected layer 2
    model.add(tf.keras.layers.Dense(4096, activation='relu', kernel_regularizer=l2(l=0.01)))
    model.add(tf.keras.layers.Dropout(0.5))

    # Activation layer
    model.add(tf.keras.layers.Dense(num_classes, activation='softmax'))

    # Compile model
    model.compile(loss=tf.keras.losses.categorical_crossentropy,
                  optimizer=tf.keras.optimizers.Adam(),
                  metrics=['accuracy'])
    return model


def fit_gen(model,
            train_iter,
            cross_val_iter,
            train_steps,
            validate_steps,
            epochs,
            callbacks=(),
            gpu=None):
    def get_fit(m): return m.fit_generator(generator=train_iter,
                                           validation_data=cross_val_iter,
                                           steps_per_epoch=train_steps,
                                           validation_steps=validate_steps,
                                           callbacks=callbacks, epochs=epochs,
                                           verbose=1, workers=1)
    fit = None
    try:
        if gpu:
            logging.info('Training NN with GPU')
            with tf.device(gpu):
                fit = get_fit(model)
        else:
            logging.info('Training NN with CPU')
            fit = get_fit(model)
    except Exception as e:
        logging.exception('Exception on fit generator function: {}'.format(e))
    return fit


def eval_gen(model, test_iter, steps, gpu=None):
    def get_eval(m): return m.evaluate_generator(test_iter, verbose=1, workers=1, steps=steps)
    score = ['N/A', 'N/A']
    try:
        if gpu:
            logging.info('Evaluating NN with GPU')
            with tf.device(gpu):
                score = get_eval(model)
        else:
            logging.info('Evaluating NN with CPU')
            score = get_eval(model)
        logging.info('Test loss: {}\nTest accuracy: {}'.format(score[0], score[1]))
    except Exception as e:
        logging.exception('Exception on eval generator function: {}'.format(e))
    return score


def save_to_fs(model, score, fp='./'):
    fn_score = str(int(score[1] * 100))
    model_fn = fp+'model-{}.hdf5'.format(fn_score)
    model.save(filepath=model_fn)
    logging.info('Model saved as: {}'.format(model_fn))


def init_callbacks(save_freq, fp='./'):
    return [
        ModelCheckpoint(filepath=fp+'checkpoint-{epoch:02d}-{val_loss:.2f}.hdf5', monitor='val_loss', verbose=1,
                        save_best_only=True, save_weights_only=False, mode='min', save_freq='epoch', period=save_freq)
    ]


def get_dataset_path(meta_df):
    source_folders = meta_df.fp.unique()
    if len(source_folders) != 1:
        raise ValueError('All data must be sourced from 1 directory for image generator to work.')
    return source_folders[0]


def process_dataset(descriptor='dataset.csv', size=(512, 384)):
    df = Utils.load_csv(descriptor)
    descriptors = []
    for s, d, l in zip(df['source'], df['dest'], df['label']):
        descriptors.append(DatasetDescriptor(src=s, dst=d, label=l, size=size))
    return DatasetSplitter().process_descriptors(descriptors)


def main(dataset=None, model=None, gpu=None, save_checkpoint_at='./', save_model_at='./'):
    train_meta_fp, test_meta_fp, validate_meta_fp = process_dataset(size=(240, 240)) if dataset is None else dataset

    train_meta_df = Utils.load_csv(train_meta_fp)
    test_meta_df = Utils.load_csv(test_meta_fp)
    validate_meta_df = Utils.load_csv(validate_meta_fp)

    train_data_fp = get_dataset_path(train_meta_df)
    test_data_fp = get_dataset_path(test_meta_df)
    validate_data_fp = get_dataset_path(validate_meta_df)

    train_iterator = augment_train_from_fp(train_meta_df, train_data_fp,
                                           batch_size=Config.TRAIN_BATCH_SIZE.value,
                                           target_size=Config.IMAGE_SHAPE.value[:-1])
    validate_iterator = augment_test_from_fp(validate_meta_df, validate_data_fp,
                                             batch_size=Config.VALIDATE_BATCH_SIZE.value,
                                             target_size=Config.IMAGE_SHAPE.value[:-1])
    test_iterator = augment_test_from_fp(test_meta_df, test_data_fp,
                                         batch_size=Config.TEST_BATCH_SIZE.value,
                                         target_size=Config.IMAGE_SHAPE.value[:-1])

    model = compile_model(Config.IMAGE_SHAPE.value, Config.NUM_CLASSES.value) if model is None else model

    fit_gen(model=model,
            train_iter=train_iterator,
            cross_val_iter=validate_iterator,
            train_steps=Config.TRAIN_STEPS.value,
            validate_steps=Config.VALIDATE_STEPS.value,
            epochs=Config.EPOCHS.value,
            callbacks=init_callbacks(save_freq=Config.EPOCHS_PER_CHECKPOINT.value, fp=save_checkpoint_at),
            gpu=gpu)

    score = eval_gen(model, test_iterator, steps=Config.TEST_STEPS.value, gpu=gpu)
    save_to_fs(model, score, fp=save_model_at)


if __name__ == '__main__':
    logging.info('Started at: {}'.format(datetime.now()))
    main(dataset=None, model=Utils.load_checkpoint(), gpu=Utils.load_gpu())
    # augment('./dataset/original/trash', 500)
    logging.info('Finished at: {}'.format(datetime.now()))
