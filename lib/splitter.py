from .utils import Utils

from enum import Enum

import logging
import cv2
import pandas as pd
from itertools import zip_longest
from sklearn.model_selection import train_test_split


class DatasetDescriptor:
    def __init__(self, src, dst, label, size=(512, 384)):
        self.src = src
        self.dst = dst
        self.label = label
        self.size = size


class DatasetEntry:
    def __init__(self, image, filename, label):
        self.image = image
        self.filename = filename
        self.label = label


class DatasetSplitter:

    class Descriptors(Enum):
        TRAIN = 'train'
        TEST = 'test'
        VALIDATION = 'validation'

    def __init__(self):
        self.descriptors = {
            DatasetSplitter.Descriptors.TRAIN: [],
            DatasetSplitter.Descriptors.TEST: [],
            DatasetSplitter.Descriptors.VALIDATION: []
        }

    @staticmethod
    def _load_dataset(fp, label, size):
        logging.info('Loading={} with label={}, resized to={}'.format(fp, label, size))
        dataset = []
        for filename in Utils.list_files(fp):
            img = Utils.load_image(fp, filename)
            if img is not None:
                img_resized = cv2.resize(src=img, dsize=size, interpolation=cv2.INTER_CUBIC)
                dataset.append(DatasetEntry(image=img_resized, filename=filename, label=label))
        return dataset

    @staticmethod
    def _split_dataset(dataset, shuffle=True):
        # Note: Using train_split_test twice is sub-optimal for large datasets:
        train, test = train_test_split(dataset, test_size=0.2, train_size=0.8, shuffle=shuffle)
        train, cross_val = train_test_split(train, train_size=0.75, test_size=0.25, shuffle=shuffle)
        return train, cross_val, test

    @staticmethod
    def _split_for(path, label, size):
        dataset = DatasetSplitter._load_dataset(path, label, size)
        train, cross_val, test = DatasetSplitter._split_dataset(dataset)
        return train, cross_val, test

    def _save_entry(self, target, entry, descriptor):
        if entry is None or descriptor is None:
            return
        if target is None or target == '':
            raise ValueError('Target entry can\'t be null or empty')
        fp = '{}/{}'.format(descriptor.dst, target.value)
        Utils.save_image(fp, entry.filename, entry.image)
        self.descriptors[target].append({
            'fp': fp,
            'filename': entry.filename,
            'labels': [descriptor.label],
        })

    def _save_metadata(self, fp, append):
        train_df = pd.DataFrame(self.descriptors[DatasetSplitter.Descriptors.TRAIN])
        validation_df = pd.DataFrame(self.descriptors[DatasetSplitter.Descriptors.VALIDATION])
        test_df = pd.DataFrame(self.descriptors[DatasetSplitter.Descriptors.TEST])

        train_meta_fp = Utils.save_df(fp, DatasetSplitter.Descriptors.TRAIN.value, train_df, append=append)
        test_meta_fp = Utils.save_df(fp, DatasetSplitter.Descriptors.TEST.value, test_df, append=append)
        validate_meta_fp = Utils.save_df(fp, DatasetSplitter.Descriptors.VALIDATION.value, validation_df, append=append)

        return train_meta_fp, test_meta_fp, validate_meta_fp

    def process_descriptors(self, dataset_descriptors, append=True):
        descriptors_fp = None
        for desc in dataset_descriptors:
            descriptors_fp = descriptors_fp if descriptors_fp is not None else desc.dst
            if not append and len(Utils.list_files(desc.dst)) > 0:
                logging.info('Appending is disabled, skipping non empty target directory={}'.format(desc.dst))
                continue
            train, cross_val, test = DatasetSplitter._split_for(desc.src, desc.label, desc.size)
            logging.info('\n... {} train samples'
                         '\n... {} cross validation samples'
                         '\n... {} test samples'.format(len(train), len(cross_val), len(test)))
            for tr, cr, te in zip_longest(train, cross_val, test):
                self._save_entry(target=DatasetSplitter.Descriptors.TRAIN, entry=tr, descriptor=desc)
                self._save_entry(target=DatasetSplitter.Descriptors.VALIDATION, entry=cr, descriptor=desc)
                self._save_entry(target=DatasetSplitter.Descriptors.TEST, entry=te, descriptor=desc)

        return self._save_metadata(descriptors_fp, append=append)
