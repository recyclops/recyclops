import Augmentor
import os


def augment(fp='', samples=500):
    if not os.path.exists(fp):
        raise ValueError('Invalid path')
    p = Augmentor.Pipeline(fp)
    p.rotate_random_90(probability=.5)
    p.rotate(probability=.1, max_left_rotation=25, max_right_rotation=25)
    p.flip_top_bottom(probability=.5)
    p.flip_left_right(probability=.5)
    p.random_distortion(probability=.2, grid_height=4, grid_width=4, magnitude=7)
    p.zoom(probability=.3, min_factor=1.1, max_factor=1.5)
    p.sample(samples)
