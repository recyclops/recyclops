import os
import logging

import cv2
import pandas as pd
import tensorflow as tf
from tensorflow.keras.models import load_model


class Utils:

    @staticmethod
    def isvalid_path(fp):
        return fp is not None and fp != ''

    @staticmethod
    def isvalid_filename(filename):
        return filename is not None and filename != ''

    @staticmethod
    def load_checkpoint(fp=''):
        if os.path.exists(fp) is False:
            return None
        model = None
        try:
            model = load_model(filepath=fp)
            model.summary()
        except Exception as e:
            logging.exception('Exception loading checkpoint: {}'.format(e))
        return model

    @staticmethod
    def load_csv(fp):
        if not Utils.isvalid_path(fp):
            logging.warning('Invalid path to load csv file')
            return None
        df = None
        try:
            df = pd.read_csv(fp)
        except IOError as ioe:
            logging.exception('Unable to load csv file from path={}, exception: {}'.format(fp, ioe))
        return df

    @staticmethod
    def load_image(fp, filename):
        if not Utils.isvalid_path(fp) or not Utils.isvalid_filename(filename):
            logging.warning('Invalid path to load image')
            return None
        image = None
        try:
            image = cv2.imread(os.path.join(fp, filename))
        except IOError as ioe:
            logging.exception('Unable to load image={} from directory={}, exception: {}'.format(filename, fp, ioe))
        return image

    @staticmethod
    def save_image(fp, filename, image):
        if not Utils.isvalid_path(fp) or not Utils.isvalid_filename(filename):
            logging.warning('Invalid path to save image/Image is null')
            return
        try:
            cv2.imwrite(os.path.join(fp, filename), image)
        except IOError as ioe:
            logging.exception('Unable to save image as={} to directory={}, exception: {}'.format(filename, fp, ioe))

    @staticmethod
    def save_df(fp, filename, df, append=False, ext='.csv'):
        if not Utils.isvalid_path(fp) or not Utils.isvalid_filename(filename):
            logging.warning('Invalid path or filename to store dataframe to')
            return
        df_filename = None
        try:
            df_filename = os.path.join(fp, filename)+ext
            mode = 'a' if append else 'w'
            df.to_csv(df_filename, encoding='utf-8', mode=mode, index=True)
        except IOError as ioe:
            logging.exception('Unable to save dataframe as={} to directory={}, exception: {}'.format(filename, fp, ioe))
        return df_filename

    @staticmethod
    def list_files(fp):
        if fp is None or fp == '':
            logging.warning('Empty directory can\'t be listed')
            return None
        files = []
        try:
            files = os.listdir(fp)
        except IOError as ioe:
            logging.exception('Unable to list directory content at={}, exception: {}'.format(fp, ioe))
        return files

    @staticmethod
    def load_gpu():
        gpu = tf.test.gpu_device_name()
        if gpu != '/device:GPU:0':
            logging.warning('GPU device not found')
            return None
        logging.info('Found GPU at: {}'.format(gpu))
        return gpu

    @staticmethod
    def format_size(size):
        x, y = size.split(":")
        return int(x), int(y)
