# recyclops


Proof of concept device to test out an idea of automated recycling process happening right after piece of garbage gets thrown away.

## Project structure

- main.py : app start point
- dataset : folder containing dataset for NN training

## Dataset

The following datasets are being used: 
- trashnet : https://github.com/garythung/trashnet

Dataset structure: 
- dataset/cardboard
- dataset/glass
- dataset/metal
- dataset/paper
- dataset/plastic
- dataset/trash

Image properties: 
- `.jpg` format
- `512x384` dimensions

## Run

`pip install -r requirments`

`python3 main.py`
