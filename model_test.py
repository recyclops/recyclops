from lib import Utils

import os

import cv2
from PIL import Image
import numpy as np
from numpy import asarray
import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.preprocessing.image import ImageDataGenerator


def load_image(fp):
    img = Image.open(fp)
    if img is not None:
        return img.convert('RGB')
    return None


def normalize_image(image):
    pixels = asarray(image)
    # confirm pixel range is 0-255
    # convert from integers to floats
    pixels = np.reshape(pixels, (1, 512, 384, 3))
    pixels = pixels.astype('float32')
    print('Data Type: %s' % pixels.dtype)
    print('Pre-normalize: Min: %.3f, Max: %.3f' % (pixels.min(), pixels.max()))
    # normalize to the range 0-1
    pixels *= (1./255)
    # confirm the normalization
    print('Post-normalize: Min: %.3f, Max: %.3f' % (pixels.min(), pixels.max()))
    return pixels


def test(model, dataset='./dataset/original', clazz='cardboard', img_id=1):
    image_fp = '{}/{}/{}.jpg'.format(dataset, clazz, clazz+str(img_id))
    image = load_image(image_fp)
    image = normalize_image(image)
    predictions = model.predict(image)
    prediction = int(np.argmax(predictions[0]))

    classes = [
        'cardboard',
        'glass',
        'metal',
        'paper',
        'plastic',
        'trash'
    ]
    print('Predicted type: {}'.format(classes[prediction]))
    return classes[prediction]


if __name__ == '__main__':
    model = Utils.load_checkpoint('./models/model.hdf5')
    clazz = 'cardboard'
    count = 0
    for i in range(1, 300):
        if test(model, clazz=clazz, img_id=i) == clazz:
            count += 1
    print('Classified', count)